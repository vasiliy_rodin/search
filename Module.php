<?php

namespace Search;

use Exception;
use Search\Service\SearchService;
use SphinxClient;
use Zend\ServiceManager\ServiceManager;

class Module
{

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	public function getServiceConfig()
	{
		return array(
			'factories' => array(

                'SearchService' => function(ServiceManager $sm)
                {

                    $config = $sm->get('Config');
                    $sphinxConfig = isset($config['sphinx']) ? $config['sphinx'] : null;

                    if (!$sphinxConfig) {
                        throw new Exception('Please, specify Sphinx config');
                    }

                    $host  = isset($sphinxConfig['host']) ? $sphinxConfig['host'] : 'localhost';
                    $port  = isset($sphinxConfig['port']) ? $sphinxConfig['port'] : 3312;
                    $limit = isset($sphinxConfig['limit']) ? $sphinxConfig['limit'] : 5;

                    $path = isset($sphinxConfig['path']) ? $sphinxConfig['path'] : '';

                    if (!file_exists(__DIR__ . $path)) {
                        throw new Exception('Please, specify path for sphinx lib');
                    }

                    $lib = __DIR__ . $path;
                    include_once($lib);

                    $sphinxClient = new SphinxClient();
                    $sphinxClient->setServer($host, $port);
                    $sphinxClient->SetMatchMode(SPH_MATCH_ALL);

                    return new SearchService($sphinxClient, $limit);
                },

			),
		);
	}

}
