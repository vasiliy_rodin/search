<?php

namespace Application\Form\Filter;


use Zend\Filter\FilterChain;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;

class Sanitize extends FilterChain
{
    public function __construct($options = array())
    {
        parent::__construct($options);
        $this->attach(new StringTrim());
        $this->attach(new StripTags());
    }
}