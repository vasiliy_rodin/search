<?

namespace Search\Result;

/**
 * Representing sphinx search result
 */
class Result
{

    protected $matches = array();
    protected $ids = array();
    protected $total = 0;

    /**
     * Constructor
     *
     * @param bool|array $sphinxResult search result, formatted by Sphinx API
     */
    public function __construct(array $sphinxResult)
    {

        // Set results if we have matches
        if (isset($result['matches'])) {
            $this->matches = $result['matches'];
            $this->ids = array_keys($result['matches']);
            $this->total = (int) $result['total_found'];
        }

    }

    /**
     * Get result ids
     *
     * @return array
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * Get search matches
     *
     * @return mixed
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Get total count of results
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

}