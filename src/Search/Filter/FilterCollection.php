<?

namespace Search\Filter;

use SphinxClient;

class FilterCollection extends \SplObjectStorage
{

    public function addSimpleFilter($key, $value)
    {
        if ($value || $value === 0) {
            $this->attach(new SimpleFilter($key, $value));
        }
    }

    public function addRangeFilter($key, $from, $to)
    {
        if ($from && $to) {
            $this->attach(new RangeFilter($key, $from, $to));
        }
    }

    public function process(SphinxClient $sphinxClient)
    {
        foreach ($this as $filter) {
            if ($filter instanceof SphinxFilter) {
                $sphinxClient = $filter->process($sphinxClient);
            }
        }
        return $sphinxClient;
    }

}