<?

namespace Search\Filter;

abstract class SphinxFilter
{

    abstract public function process (\SphinxClient $sphinxClient);

}