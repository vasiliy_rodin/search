<?

namespace Search\Filter;

use SphinxClient;

class SimpleFilter extends SphinxFilter
{

    protected $value;
    protected $key;

    public function __construct($key, $value)
    {
        $this->key   = $key;
        $this->value = $value;
    }

    public function process(SphinxClient $sphinx)
    {
        $sphinx->setFilter($this->key, is_array($this->value) ? $this->value : array($this->value));
        return $sphinx;
    }

}