<?

namespace Search\Filter;

use SphinxClient;

class RangeFilter extends SphinxFilter
{

    protected $from;
    protected $to;
    protected $key;

    public function __construct($key, $from, $to)
    {
        $this->key = $key;
        $this->from = $from;
        $this->to = $to;
    }

    public function process(SphinxClient $sphinx)
    {
        $sphinx->setFilterRange($this->key, $this->from, $this->to);
        return $sphinx;
    }

}