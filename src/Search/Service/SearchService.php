<?

namespace Search\Service;

use Application\Form\Filter\Sanitize;
use Search\Filter\FilterCollection;
use Search\Result\Result;
use SphinxClient;

class SearchService
{

    private $sphinxClient;
    protected $limit;

    protected $query;
    protected $filter;

    public function __construct(SphinxClient $sphinxClient, $limit)
    {
        $this->sphinxClient = $sphinxClient;
        $this->limit = $limit;
        $this->filter = new Sanitize();
    }

    /**
     * Perform search
     *
     * @param string           $query
     * @param int              $offset
     * @param string           $index
     * @param FilterCollection $filterCollection
     * @param int              $limit
     * @param string           $sortBy
     * @param string           $order
     *
     * @return array
     */
    public function search($query,
                           $offset = 0,
                           $index = '*',
                           FilterCollection $filterCollection = null,
                           $limit = null,
                           $sortBy = null,
                           $order = 'DESC')
    {
        $this->query = $this->filter->filter(urldecode($query));

        $sphinxClient = $this->sphinxClient;

        if ($filterCollection) {
            $sphinxClient = $filterCollection->process($sphinxClient);
        }

        $limit = $limit ? : $this->limit;

        $sphinxClient->SetLimits((int) $offset, $limit);

        if ($sortBy) {
            $mode = ($order == 'DESC') ? SPH_SORT_ATTR_DESC : SPH_SORT_ATTR_ASC;
            $sphinxClient->SetSortMode($mode, $sortBy);
        }

        // Simple implementation of query
        $finalQuery = '*'.$this->query.'*';

        // Full scan for empty queries
        if ($this->query == '') {
            $sphinxClient->SetMatchMode(SPH_MATCH_FULLSCAN);
            $finalQuery = '*';
        }

        $result = $sphinxClient->Query($finalQuery, $index);

        // Return formatted result
        return new Result($result);
    }

    public function getQuery()
    {
        return $this->query;
    }
}
