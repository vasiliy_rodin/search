<?

namespace Search\Controller;

use Search\Filter\FilterCollection;
use Search\Service\SearchService;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{

    /**
     * Sample using of search
     */
    public function indexAction()
    {
        /** @var SearchService $searchService */
        $searchService = $this->getServiceLocator()->get('SearchService');

        // We can use filters for search
        // In this example - group_id = 5 and age between 16 and 30
        $filterCollection = new FilterCollection();
        $filterCollection->addSimpleFilter('group_id', 5);
        $filterCollection->addRangeFilter('age', 16, 30);

        // Search query
        $result = $searchService->search('query', 0, '', $filterCollection);

        // Render ViewModel with result
        return array(
            'result' => $result
        );
    }

}
